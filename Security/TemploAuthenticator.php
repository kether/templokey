<?php

namespace EstudioHecate\Bundle\TemploKeyBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client as GuzzleClient;
use Doctrine\ORM\EntityManagerInterface;

class TemploAuthenticator implements SimpleFormAuthenticatorInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    
    /**
     * @var EntityManagerInterface
     */
    private $em;
    
    /**
     * @var GuzzleClient
     */
    private $guzzleClient;
    
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $em)
    {
        $this->encoder  = $encoder;
        $this->em       = $em;
    }
    
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        try {
            $r = $this->getGuzzleClient()->post('login', array('json' => array('usuario' => $token->getUsername(), 'clave' => $token->getCredentials())));
            
        } catch(RequestException $e) {
            if ($e->hasResponse())
                $r = $e->getResponse();
        }
        
        if(isset($r) && $r->getStatusCode() == 200) {
            $apiKey = json_decode($r->getBody())->apikey;
            $json = json_decode($this->getGuzzleClient()->post('user_info', ['json' => [], 'headers' => ['apikey' => $apiKey]])->getBody());
            
            try {
                $jugador = $userProvider->loadUserByUsername($token->getUsername());
            } catch (UsernameNotFoundException $e) {
                $className = $userProvider->getClassName();
                $jugador = new $className();
            }
            
            $jugador
                ->setApikey($apiKey)
                ->setUsuario($json->usuario)
                ->setIsAdmin($json->super)
                ->setSeudonimo($json->seudonimo)
                ->setEmail($json->email);
            
            $this->getDoctrineManager()->persist($jugador);
            $this->getDoctrineManager()->flush();
            
            return new UsernamePasswordToken($jugador, null, $providerKey, $jugador->getRoles());
        }
        
        throw new UsernameNotFoundException('El nombre de usuario o la contraseña no es válida.');
    }
    
    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof UsernamePasswordToken && $token->getProviderKey() === $providerKey;
    }
    
    public function createToken(Request $request, $username, $password, $providerKey)
    {
        return new UsernamePasswordToken($username, $password, $providerKey);
    }
    
    public function getDoctrineManager() : EntityManagerInterface
    {
        return $this->em;
    }
    
    public function getGuzzleClient() : GuzzleClient
    {
        if(!($this->guzzleClient instanceOf GuzzleClient)) {
            if(getenv('LOGINTDH_URL')) {
                $this->guzzleClient = new GuzzleClient(['base_uri' => getenv('LOGINTDH_URL')]);
            } else {
                $this->guzzleClient = new GuzzleClient();
            }
        }
        
        return $this->guzzleClient;
    }
    
    public function setGuzzleClient(\GuzzleHttp\Client $gc) : self
    {
        $this->guzzleClient = $gc;
        
        return $this;
    }
}