<?php

namespace EstudioHecate\Bundle\TemploKeyBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class TemploGuardApiAuthenticator extends AbstractGuardAuthenticator
{
    public function supports(Request $request)
    {
        // if there is already an authenticated user (likely due to the session)
        // then return false and skip authentication: there is no need.
        if ($this->security->getUser()) {
            return false;
        }
            
        // the user is not logged in, so the authenticator should continue
        return true;
    }
    
    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser(). Returning null will cause this authenticator
     * to be skipped.
     */
    public function getCredentials(Request $request)
    {
        if(!$this->supports($request)) {
            return;
        }
        
        $token = $request->headers->get('X-AUTH-TOKEN', $request->get('token', null));
        
        // What you return here will be passed to getUser() as $credentials
        return array(
            'token' => $token,
        );
    }
    
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $apiKey = $credentials['token'];
        
        if (null === $apiKey) {
            return;
        }
        
        // if a User object, checkCredentials() is called
        return $userProvider->loadUserByApikey($apiKey);
    }
    
    public function checkCredentials($credentials, UserInterface $user)
    {
        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case
        
        // return true to cause authentication success
        return true;
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }
    
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
            
            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        );
        
        return new JsonResponse($data, Response::HTTP_FORBIDDEN, ['Access-Control-Allow-Origin' => '*']);
    }
    
    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            // you might translate this message
            'message' => 'La autenticación es requerida'
        );
        
        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED, ['Access-Control-Allow-Origin' => '*']);
    }
    
    public function supportsRememberMe()
    {
        return false;
    }
}