<?php

namespace EstudioHecate\Bundle\TemploKeyBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TemploKeyBundle extends Bundle
{
    const VERSION = '1.0';
}