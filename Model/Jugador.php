<?php

namespace EstudioHecate\Bundle\TemploKeyBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Jugador
 *
 * @ORM\MappedSuperclass
 */
abstract class Jugador implements UserInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=255, unique=true)
     */
    protected $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="seudonimo", type="string", length=255)
     */
    protected $seudonimo;

    /**
     * @var string
     *
     * @ORM\Column(name="apikey", type="string", length=255, unique=true)
     */
    protected $apikey;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_admin", type="boolean")
     */
    protected $isAdmin = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime")
     */
    protected $creado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="actualizado", type="datetime")
     */
    protected $actualizado;

    public function __toString()
    {
        return $this->getUsuario();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     *
     * @return self
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set seudonimo
     *
     * @param string $seudonimo
     *
     * @return self
     */
    public function setSeudonimo($seudonimo)
    {
        $this->seudonimo = $seudonimo;

        return $this;
    }

    /**
     * Get seudonimo
     *
     * @return string
     */
    public function getSeudonimo()
    {
        return $this->seudonimo;
    }

    /**
     * Set apikey
     *
     * @param string $apikey
     *
     * @return self
     */
    public function setApikey($apikey)
    {
        $this->apikey = $apikey;

        return $this;
    }

    /**
     * Get apikey
     *
     * @return string
     */
    public function getApikey()
    {
        return $this->apikey;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set creado
     *
     * @param \DateTime $creado
     *
     * @return self
     */
    public function setCreado($creado)
    {
        $this->creado = $creado;

        return $this;
    }

    /**
     * Get creado
     *
     * @return \DateTime
     */
    public function getCreado()
    {
        return $this->creado;
    }

    /**
     * Set actualizado
     *
     * @param \DateTime $actualizado
     *
     * @return self
     */
    public function setActualizado($actualizado)
    {
        $this->actualizado = $actualizado;

        return $this;
    }

    /**
     * Get actualizado
     *
     * @return \DateTime
     */
    public function getActualizado()
    {
        return $this->actualizado;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     *
     * @return self
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    public function getRoles()
    {
        if($this->getIsAdmin())
        {
            return ['ROLE_SUPER_ADMIN'];
        }
        else
        {
            return ['ROLE_USER'];
        }
    }

    /**
     * @see self::getUsuario()
     */
    public function getUsername()
    {
        return $this->getUsuario();
    }

    public function getPassword()
    {
        return sha1($this->getUsuario().$this->getId());
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials() {
        // @todo
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function preSave()
    {
        if(is_null($this->apikey)) {
            $this->apikey = '';
        }

        if(is_null($this->creado)) {
            $this->creado = new \DateTime();
        }
        
        $this->actualizado = new \DateTime();

        if(!$this->seudonimo) {
            $this->seudonimo = '';
        }
    }
}
