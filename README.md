# Instalación de TemploKey 1.0

### Paso 0: Agregar repositorio

Primero tenemos que añadir el siguiente código JSON repositorio en el `composer.json`
de tu proyecto. Además debemos asegurarnos que tenemos acceso mediante una clave SSH
en tu ordenador al repositorio de bitbucket (de lo contrario composer dará un error). 

```json
"repositories" : [{
            "type" : "vcs",
            "url" : "git@bitbucket.org:kether/templokey.git"
        }
    ]
```


Aplicaciones que no usan Symfony Flex
-------------------------------------

### Paso 1: Descargar el Bundle

Abre la consola, entra en el directorio de tu proyecto y ejecuta
el siguiente comando para descargar la última versión de éste bundle:

```console
$ composer require estudiohecate/templokey-bundle
```

Este comando requiere que tengas Composer instalado globalmente,
 como se explica en el [capítulo de instalación](https://getcomposer.org/doc/00-intro.md)
 de la documentación de Composer.

### Paso 2: Activar el Bundle

Para activar el bundle añade a la lista de bundles registrados en
el `app/AppKernel.php` de tu proyecto el de TemploKeyBundle: 

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new EstudioHecate\Bundle\TemploKeyBundle\TemploKeyBundle(),
        );

        // ...
    }

    // ...
}
```