<?php

namespace EstudioHecate\Bundle\TemploKeyBundle\Repository;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Doctrine\ORM\EntityRepository;

/**
 * JugadorRepository
 */
class JugadorRepository extends EntityRepository implements UserProviderInterface
{
    public function loadUserByApikeyOrUsername($userOrApikey)
    {
        return $this->getEntityManager()
            ->createQuery('SELECT j FROM '.$this->getClassName().' j WHERE j.apikey = :apikey OR j.usuario = :usuario')
            ->setParameter(':apikey', $userOrApikey)
            ->setParameter(':usuario', $userOrApikey)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
    
    /**
     * @param string $apikey
     * @return object
     */
    public function loadUserByApikey($apikey)
    {
        return $this->getEntityManager()
            ->createQuery('SELECT j FROM '.$this->getClassName().' j WHERE j.apikey = :apikey')
            ->setParameter(':apikey', $apikey)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritDoc}
     * @see \Symfony\Component\Security\Core\User\UserProviderInterface::loadUserByUsername()
     */
    public function loadUserByUsername($username)
    {
        $usuario = $this->loadUserByApikeyOrUsername($username);

        if(!$usuario) {
            throw new UsernameNotFoundException(sprintf('Usuario "%s" no encontrado', $username));
        }

        return $usuario;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                    sprintf(
                            'Instances of "%s" are not supported.',
                            $class
                    )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class || is_subclass_of($class, $this->getEntityName());
    }


}
